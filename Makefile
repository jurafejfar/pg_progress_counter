MODULES = progress_counter

EXTENSION = progress_counter
DATA = progress_counter--1.0.sql
PGFILEDESC = "Simple counter for batch operations"

REGRESS = progress_counter

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
