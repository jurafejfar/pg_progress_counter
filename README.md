# pg_progress_counter

PostgreSQL extension for monitoring long running SQL queries. Based on blog article https://okbob.blogspot.com/2010/10/simple-counter-for-batch-operations.html by Pavel Stěhule.