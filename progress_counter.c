#include "postgres.h"
#include "funcapi.h"
#include "utils/lsyscache.h"
#include <time.h>

PG_MODULE_MAGIC;
PGDLLEXPORT Datum pst_counter(PG_FUNCTION_ARGS);
PG_FUNCTION_INFO_V1(pst_counter);

Datum pst_counter(PG_FUNCTION_ARGS);

typedef struct
{
 long int iterations;
 int freq;
 Oid  typoutput;
} counter_cache;

/*
 * raise notice every n call,
 * returns input without change
 */
Datum
pst_counter(PG_FUNCTION_ARGS)
{
 Datum value = PG_GETARG_DATUM(0);
 counter_cache *ptr = (counter_cache *) fcinfo->flinfo->fn_extra;
 time_t rawtime;
 struct tm * timeinfo;
 char buffer [80];

 if (ptr == NULL)
 {

  fcinfo->flinfo->fn_extra = MemoryContextAlloc(fcinfo->flinfo->fn_mcxt,
          sizeof(counter_cache));
  ptr = (counter_cache *) fcinfo->flinfo->fn_extra;
  ptr->iterations = 0;
  ptr->typoutput = InvalidOid;

  if (PG_ARGISNULL(1))
   elog(ERROR, "second parameter (output frequency) must not be NULL");

  ptr->freq = PG_GETARG_INT32(1);

  if (!PG_ARGISNULL(2) && PG_GETARG_BOOL(2))
  {
   Oid valtype;
   Oid typoutput;
   bool typIsVarlena;

   valtype = get_fn_expr_argtype(fcinfo->flinfo, 0);
   getTypeOutputInfo(valtype, &typoutput, &typIsVarlena);
   ptr->typoutput = typoutput;
  }
 }
 
 if (++ptr->iterations % ptr->freq == 0)
 {
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  strftime (buffer, 80, "%c", timeinfo);
  if (!OidIsValid(ptr->typoutput))
  {
    elog(NOTICE, "processed %ld rows", ptr->iterations);
  }
  else
  {
   /* show a processed row, when it's requested */
   if (PG_ARGISNULL(0))
    elog(NOTICE, "%s processed %ld rows, current value is null", buffer, ptr->iterations);
   else
   {
    elog(NOTICE, "%s processed %ld rows, current value is '%s'", buffer, ptr->iterations,
              OidOutputFunctionCall(ptr->typoutput, value));
   }
  }
 }

 PG_RETURN_DATUM(value);
}
