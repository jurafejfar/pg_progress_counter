CREATE EXTENSION progress_counter;

SELECT setseed(1.0);
with w_random as (
    select random() as rn from generate_series(1,1000000)
)
select sum(rn)/count(rn) as result from w_random;

SELECT setseed(1.0);
with w_random as (
    select random() as rn from generate_series(1,1000000)
)
select sum(counter(rn, 100000, false))/count(rn) as result from w_random;

SELECT setseed(1.0);
with w_random as (
    select random() as rn from generate_series(1,1000000)
)
select sum(rn)/count(rn) as result from w_random where counter(true, 100000, false);
